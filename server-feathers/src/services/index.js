const posts = require('./posts/posts.service.js');
const temas = require('./temas/temas.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(posts);
  app.configure(temas);
};
