// Initializes the `temas` service on path `/temas`
const createService = require('feathers-nedb');
const createModel = require('../../models/temas.model');
const hooks = require('./temas.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/temas', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('temas');

  service.hooks(hooks);
};
