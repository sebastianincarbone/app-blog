const assert = require('assert');
const app = require('../../src/app');

describe('\'temas\' service', () => {
  it('registered the service', () => {
    const service = app.service('temas');

    assert.ok(service, 'Registered the service');
  });
});
