import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: () => import('./views/About.vue')
    },
    {
      path: '/singIn',
      name: 'Root',
      component: () => import('./views/NewPost.vue')
    },
    {
      path: '/post/:id',
      component: () => import('./views/Post.vue')
    }
  ]
})