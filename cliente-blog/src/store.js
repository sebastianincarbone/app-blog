import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    bgColor: "red",
    temaABuscar: "all",
    post:{
      titulo:"",
      temas: [],
      descripcion:" ",
      cuerpo: "",
      _id: ""
    }
  },
  mutations: {
    EDITARPOST: (state, post) => {
      state.post = post
    },
    UPDATE: (state, color) => {
      state.bgColor = color
    },
    BUSCAR: (state, tema) => {
      state.temaABuscar = tema
    }
  }
})